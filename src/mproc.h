#ifndef __X_MAINPROC_DEFINED__
#define __X_MAINPROC_DEFINED__

#include <stdio.h>
#include "sstream.h"

//Main entry for the rest of the functions
int m_entry(char *, sstream **, int, int, int *, int);

//Functions for parsing lines
int m_compute(char *, sstream **, int, const char *);	//Parsing of equations to low-level code
int m_fnlogic(char *, sstream **, int);              	//Parsing of keywords in line
int m_declvar(char *, sstream **, int, int *, int);  	//Declaration of variables

#endif