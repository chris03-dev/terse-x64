//Custom string functions

/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "macro.h"
#include "strproc.h"

void trimw(char *si) {
	if (strlen(si)) {
		char buf[256];
		tokenzr(buf, si);
		trim(si, strlen(buf));
	}
}
void trim(char *si, unsigned int offset) {
	unsigned int len = strlen(si);
	
	//Remove whitespace from last chars
	while (len and isspace(si[len - 1])) si[len-- - 1] = '\0';
	
	//Remove whitespace from first chars
	if (len) {
		while (isspace(si[offset])) offset++;
		if (offset > strlen(si)) offset = strlen(si);
		
		char *buf = calloc(len - offset + 1, 1);
		strcpy(buf, si + offset);
		strcpy(si, buf);
		free(buf);
	}
}
void tokenzr(char *si, const char *so) {
	unsigned int len = strlen(so);
	
	char buf[256];
	strncpy(buf, so, 256);
	trim(buf, 0);
	
	for (unsigned int i = 0; i < len; ++i) {
		if (buf[0] == '"') {
			si[i] = buf[i];
			if (buf[i] == '"' and i > 0) {
				si[i + 1] = '\0';
				return;
			}
		}
		else if (isspace(buf[i])) {
			si[i] = '\0';
			return;
		}
		else si[i] = buf[i]; 
	}
}
int find(const char *si, const char *so) {
	unsigned int x = 0;		//String comparison index
	
	for (unsigned int i = 0; si[i]; ++i) {
		//Char comparison success
		if (si[i] == so[x]) {
			if (x == strlen(so) - 1) return i - x;
			else x++;
		}
		//Char comparison fail
		else x = 0;
	}
	return strlen(si);
}
int findeq(const char *si, const char *so) {
	unsigned int x = 0;		//String comparison index
	
	if (strlen(si) == strlen(so))
	for (unsigned int i = 0; si[i]; ++i) {
		//Char comparison success
		if (si[i] == so[x]) {
			if (x == strlen(so) - 1) return i - x;
			else x++;
		}
		//Char comparison fail
		else x = 0;
	}
	return strlen(si);
}
int findc(const char *si, const char c) {
	for (unsigned int i = 0; si[i]; ++i) {
		//Char comparison
		if (si[i] == c) return i;
	}
	return strlen(si);
}
int findb(const char *si, const char *so, const char blcs, const char blce, const int b) {
	unsigned int 
	x = 0,           	//String comparison index
	isinside = 0;    	//Boolean if inside a block
	
	for (unsigned int i = 0; si[i]; ++i) {
		//Check if char is entering/exiting block
		if (so[x] != blcs and si[i] == blcs) isinside = 1; else
		if (so[x] != blce and si[i] == blce) isinside = 0;
		
		//Char comparison success
		if (si[i] == so[x] and (isinside == b)) {
			if (not so[x + 1]) return i - x;
			else x++;
		}
		//Char comparison fail
		else x = 0;
		
		//printf("so[x]:'%1c'     ISSTR:%i\n", so[x], isstr);
	}
	return strlen(si);
}
int findcb(const char *si, const char c, const char blcs, const char blce, const int b) {
	int isinside = 0;	//Boolean if inside a block
	
	for (unsigned int i = 0; si[i]; ++i) {
		//Check if string character
		if (c != blcs and si[i] == blcs) isinside = 1; else
		if (c != blce and si[i] == blce) isinside = 0;
		
		//Char comparison success
		if (si[i] == c and isinside == b)
			return i;
	}
	return strlen(si);
}

int countcb(const char *si, const char c, const char blcs, const char blce, const int b) {
	int 
	isinside = 0,	//Boolean if inside a block
	count = 0;   	//Number of characters
	
	for (unsigned int i = 0; si[i]; ++i) {
		//Check if string character
		if (c != blcs and si[i] == blcs) isinside = 1; else
		if (c != blce and si[i] == blce) isinside = 0;
		
		//Char comparison success
		if (si[i] == c and isinside == b)
			count++;
	}
	return count;
}

int isnum(const char *si) {
	unsigned int len = strlen(si);
	
	//Hexadecimal
	if (find(si, "0x") < 1) {
		for (unsigned int i = 2; i < len; ++i)
			if (not isxdigit(si[i])) return 0;
	} 
	//Binary
	else if (find(si, "0b") < 1) {
		for (unsigned int i = 2; i < len; ++i)
			if (findc("01", si[i])) return 0;
	}
	//Number
	else for (unsigned int i = 0; i < len; ++i) 
		if (not isdigit(si[i])) return 0;
		
	//Number success
	return 1;
}

unsigned char 
	flag_mcom = 0,	//Multi-line comment flag
	flag_scom = 0;	//Single-line comment flag

void preproc(char *si) {
	int
		len = strlen(si),
		loc_slc = findcb(si, '#', '"', '"', 0), 	// Location for single line comment
		loc_mlcs = findb(si, "_<", '"', '"', 0),	// Location for multiline comment start
		loc_mlce = findb(si, ">_", '"', '"', 0),	// Location for multiline comment end
		loc_bsl = findcb(si, '\\', '"', '"', 0);	// Location for escape char (single line)
	
	//Set flags
	if (not flag_mcom) {
		if (loc_slc < len)  flag_scom = 1;
		if (loc_mlcs < len) flag_mcom = 1;
	
		//Resolve flag conflicts
		if (loc_mlcs < loc_slc) {
			flag_scom = 0;
			loc_slc = len;
		}
		if (loc_slc < loc_mlcs) {
			flag_mcom = 0;
			loc_mlcs = len;
		}
	}
	
	//Comment conditions; They should be the ones turning off the flags
	if (flag_scom) {
		if (loc_bsl == len) flag_scom = 0;
		si[loc_slc * (loc_slc < len)] = '\0';
	}
	if (flag_mcom) {
		if (loc_mlcs < len) si[loc_mlcs] = 0;
		else if (loc_mlce < len) {
			trim(si, loc_mlce + 2);
			flag_mcom = 0;
		}
		else si[0] = '\0';
	}
}