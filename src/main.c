//Main code

/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "strproc.h"
#include "macro.h"
#include "sstream.h"
#include "mproc.h"

int main(int argc, char ** argv) {
	setbuf(stdout, 0);
	unsigned int argn = 1;          	//Argument number
	unsigned int memc = 256;        	//String memory
	
	//Flags
	int
	flag_vrbs = 0,          	//Flag for verbose, showing internal data
	flag_outf = 0,          	//Flag if output already included
	flag_trgt = OS_DEFAULT, 	//Flag for target operating system
	flag_trgt_cmp = OS_DEFAULT; //Flag for current target operating system in code
	
	//Allocate memory for variable id
	sstream
		v_s, 	//Signed variables
		v_u, 	//Unsigned variables
		v_f, 	//Float variables
		v_b, 	//8 bit variables
		v_w, 	//16 bit variables
		v_d, 	//32 bit variables
		v_q, 	//64 bit variables
		v_c, 	//Executable code
		v_ct;	//Executable code (temporary)
	
	//...not much purpose, but DO NOT DELETE IT
	puts("");
	
	//Check if arguments included
	if (argc > 1) {
		//printf("Default OS target: %s\n", (flag_trgt == OS_POSIX) ? "*nix" : "Windows");
		
		//Read arguments
		while (argn < argc) {
			//Flag condition
			if (argv[argn][0] == '-' and not flag_outf) {
				char
				argflag[16],         	//Flag argument
				flag = argv[argn][1];	//Flag prefix + argument
				
				unsigned int argflen;	//Length of argflag
				
				strcpy(argflag, argv[argn]);
				trim(argflag, 2);
				argflen = strlen(argflag);
				
				//Choose argument
				switch (flag) {
					//Verbose argument
					case '-':
						if (find(argflag, "ascii") == 0
						or  find(argflag, "utf-8") == 0
						or  find(argflag, "utf8")  == 0) {
							if (find(argflag, "ascii") == 0) puts("ASCII is dead, long live UTF-8.\n");
							return 0;
						}
						if (find(argflag, "help") == 0)     goto j_flag_help;
						if (find(argflag, "verbose") == 0)  goto j_flag_vrbs;
						if (find(argflag, "target") == 0
						or  find(argflag, "platform") == 0) 
							goto j_flag_pltf;
						
						break;
					//Manual about program usage
					case 'h':
					j_flag_help:
						puts("terse compiler");
						puts("Copyright:    chris03-dev");
						puts("Version:      0.1.0c (WORK IN PROGRESS)");
						puts("Usage:        terse <flag> ... <ofile> <ifile> <...>\n");
						puts("Flag    Description                              Args");
						puts("-c      Set target CPU architecture              str");
						puts("-h      ...you already used this flag to read    N/A");
						puts("        this whole text");
						puts("-m      Set maximum memory for file reading      int");
						puts("        (in bytes times 20)");
						puts("-p -t   Set target OS for assembly output        str");
						puts("-s      Compile to assembly code only            N/A");
						puts("-v      Expose compiler data upon operation      N/A");
						puts("        Includes boolean values, lines read,");
						puts("        tokens identified, etc.\n");
						return 0;
						
					//Compile flags
					case 'm':
						if (atoi(argflag) >= 128) {
							memc = atoi(argflag);
							
							printf("Set maximum string memory to %.2f kb.\n", (double) atoi(argflag) * 20/1000);
							if (memc % 32) puts("Warning: Memory is not divisible by 32 bytes, may cause undefined behaviour.\n");
						}
						else puts("Ignored invalid memory size, must be at least 128 bytes.\n");
						break;
					case 'p':
					case 't':
					j_flag_pltf:
						//Check for keywords
						if ((find(argflag, "nix")   == 0 and argflen == 3)
						or  (find(argflag, "unix")  == 0 and argflen == 4)
						or  (find(argflag, "linux") == 0 and argflen == 5)) {
							
							puts("Set OS target to Linux/UNIX.");
							flag_trgt     = OS_POSIX;
							flag_trgt_cmp = OS_POSIX;
						}
						
						else
						if ((find(argflag, "gnu")   == 0 and argflen == 3)
						or  (find(argflag, "gnulinux")  == 0 and argflen == 8)
						or  (find(argflag, "gnu/linux") == 0 and argflen == 9)) {
							puts("Set OS target to GNU/Linux.\n");
							flag_trgt     = OS_LINUX;
							flag_trgt_cmp = OS_LINUX;
							
							// Rant rant rant rant
							puts("But wait! GNU is a software suite by the GNU/community");
							puts("under the GNU/GPL. Linux is a kernel, and in extension");
							puts("the operating system, separate from the GNU, and also");
							puts("under the GNU/GPL (version 2).\n");
							
							puts("Most GNU/software is forced to rely on its environment,");
							puts("Linux, Unix, BIOS, UEFI, Windows or otherwise, in order");
							puts("to function. The Linux kernel, however, isn't forced to");
							puts("rely on GNU/software, or in this case, bootloaders like");
							puts("GNU/GRUB(2).\n");
							
							puts("Naming a work from a different entity under another name");
							puts("because of its license is ridiculous in my point of view.");
							puts("I understand that most of the GNU/software that has been");
							puts("made over the years were by voluntary GNU/contributors who");
							puts("share the philosophy of developing, running and modifying");
							puts("UNIX-like GNU/free software, to which the GNU/(L)GPL");
							puts("licenses have provided them the means to do so, and");
							puts("cemented in the GNU/gnu.org webpages.\n");
							
							puts("But Linux is free software under the GNU/GPL, but it must");
							puts("be made clear that it's not part of GNU itself. Linus made");
							puts("the choice of using the GNU/GPLv2 license as he wanted all");
							puts("modifications of the kernel (and its forks) to be available");
							puts("to the public, and not much else, really. Nothing about the");
							puts("GNU/licenses state that the GNU is allowed to just insert a");
							puts("'GNU' trademark/brand over your OWN brand or name. Imagine if");
							puts("Apache brands LLVM as 'Apache/LLVM'. Or Amazon decides to brand");
							puts("all of its software as Amazon/<insert stolen GNU/GPL software");
							puts("with an Amazon brand attached to it>.\n");
							
							puts("Linux doesn't represent the GNU operating system because");
							puts("it does NOT apply all of the GNU/intentions in mind; it");
							puts("has legally allowed tivoization with the GNU/GPLv2 license,");
							puts("something that the GNU has discouraged with the release of");
							puts("the GNU/(L)GPLv3 licenses. It also allows running and hosting");
							puts("of, god forbid, proprietary software.\n");
							
							puts("GNU already has GNU/Hurd for the 'kernel' part of the GNU");
							puts("suite to fulfill such a task. GNU only 'branded' Linux as");
							puts("GNU/Linux because it is accesible to more users. GNU is not");
							puts("UNIX, and Linux is not UNIX, but that would not, will not,");
							puts("and shall not mean that GNU is Linux, because in the end,");
							puts("someone else made that software, and they and contributors");
							puts("are not all necessarily part of the GNU/community.\n");
							
							puts("Tired of using the 'GNU/' badge yet? Well, I have news");
							puts("for you. Linux should be called Linux, Inkscape should");
							puts("be called Inkscape, MuseScore should be called MuseScore,");
							puts("and your own software should be called what you want to");
							puts("call it.\n");
							
							puts("For the GNU/fanatics out there, please get it through your");
							puts("GNU/head, or just ignore it. Or start a flame war about it");
							puts("on HackerNews or Reddit. Your call.\n");
							
							puts("(If you want to save yourself from the insanity of reading");
							puts("this rant every single time, please use '-plinux', '-punix',");
							puts("or '-pnix' instead, or delete this text in your own fork of");
							puts("this particular software.)\n");
							
							puts("Press the ENTER key to continue.");
							getchar();
						}
						
						else
						if ((find(argflag, "w32")     == 0 and argflen == 3)
						or  (find(argflag, "win")     == 0 and argflen == 3)
						or  (find(argflag, "win32")   == 0 and argflen == 5)
						or  (find(argflag, "windows") == 0 and argflen == 7)) {
							
							puts("Set OS target to Microsoft Windows.");
							flag_trgt     = OS_WIN32;
							flag_trgt_cmp = OS_WIN32;
							
						}
						else {
							printf("Ignored invalid platform target '%s'.\n", argflag);
						}
						
						break;
					case 'v':
					j_flag_vrbs:
						if (flag_vrbs) {puts("Verbose mode disabled."); flag_vrbs = 0;}
						else           {puts("Verbose mode enabled.");  flag_vrbs = 1;}
						break;
					//Work in progress flags. Do not move until implemented
					case 'c':
						puts("No such feature yet. Work in progress!");
						break;
					default:
						printf("Ignored invalid flag '-%c'.\n", flag);
						break;
				}
			}
			
			//File output condition
			else if (not flag_outf) {
				flag_outf = argn;
				printf("Set '%s' as output file.\n\n", argv[argn]);
				
				//Declare sets
				v_s = sstreamdef(memc << 2);
				v_u = sstreamdef(memc << 2);
				v_f = sstreamdef(memc << 2);
				v_b = sstreamdef(memc);
				v_w = sstreamdef(memc);
				v_d = sstreamdef(memc);
				v_q = sstreamdef(memc);
				v_c = sstreamdef(memc << 2);
			}
			
			//File input condition
			else {
				//For initialization
				FILE *inf;           	//Single file input
				char line[256] = {0};	//Single line
				
				//Analyze lines
				sstream *v_list[8] = {&v_s, &v_u, &v_f, &v_b, &v_w, &v_d, &v_q, &v_c};
				
				int
				flag_fnc = 0, 	//Flag for treating data as functions
				flag_dcl = 0, 	//Flag for finding declaration state
				flag_asm = 0; 	//Flag for leaving line as is
				//flag_blc = 0, 	//Flag for if, else, while blocks
				//flag_blci = 0;	//Increment for if, else, while blocks; Should be reset after function ends
				
				//Open file
				{
					inf = fopen(argv[argn], "r");
					printf("Opening '%s'...\n", argv[argn]);
					
					if (not inf) {
						puts("Error: No such file or directory.");
						return 1;
					}
					else {
						unsigned int i = 1;	//Line number
						
						//Read line
						while (fgets(line, 256, inf)) {
							if (strlen(line) > 1) {
								//Preprocessing the line for suitable lexing
								preproc(line);
								trim(line, 0);
								
								if (strlen(line)) {
									//Process line
									char token[16] = {0};
									int read_state = 0;		//Flexible variable to determine compiler errors
									
									//Set flags by checking for keywords
									tokenzr(token, line);
									
									//Flag set
									unsigned int wlen = strlen(token);
									
									//Program flags
									switch (wlen) {
										case 5: 
											if (findb(token, "endsm", '"', '"', 0) < wlen) flag_asm = 0; 
											break;
										case 3: 
											if (findb(token, "ent", '"', '"', 0) < wlen)   flag_fnc = 1;
											if (findb(token, "ret", '"', '"', 0) < wlen)   flag_fnc = 0;
											if (findb(token, "asm", '"', '"', 0) < wlen)   flag_asm = 1;
											break;
										case 2: 
											if (findb(token, "fn", '"', '"', 0) < wlen)    flag_fnc = 1;
											break;
									}
									
									//Target OS flags
									switch (wlen) {
										case 6: 
											if (findb(token, "target", '"', '"', 0) < wlen) {
												char linecpy[256];
												strcpy(linecpy, line);
												trim(linecpy, strlen(token));
												
												if (find(linecpy, "nix") == 0) flag_trgt_cmp = OS_POSIX;
												if (find(linecpy, "w32") == 0) flag_trgt_cmp = OS_WIN32;
											}
											break;
										case 4: 
											if (findb(token, "endt", '"', '"', 0) < wlen) 
												flag_trgt_cmp = OS_DEFAULT;
											break;
									}
									
									//Check if platform is equal to target
									if (flag_trgt == flag_trgt_cmp) {
										//Show line data
										if (flag_vrbs) 
											printf("%04i: (%.4i : %.4i) '%s' \n", i, v_c.index[v_c.size], v_c.ssize, line);
										
										//Check if directly write or parse line
										if (flag_asm and findb(token, "asm", '"', '"', 0) == strlen(token)) {
											strcat(line, "\n");
											ssgets(&v_c, line);
										}
										else if (findb(token, "endsm", '"', '"', 0) == strlen(token)) {
											//Print line (and show if given errors)
											
											//Entry to parsing strings
											read_state = m_entry(line, v_list, flag_trgt, flag_fnc, &flag_dcl, flag_vrbs);
											
											if (read_state) {
												puts("");
												return 1;
											}
										}
									}
								}
							}
							++i;
						}
					}
					
					if (inf != NULL) fclose(inf);
				}
				
				puts("Finished file read.\n");
			}
			
			//Increment
			argn++;
		}
		
		//Output assembly to file
		if (flag_outf) {
			char
			bytesz = 0,
			name[32] = {0},
			arrsz[4] = {0},
			value[128] = {0};
			
			printf("Generating assembly code... ");
			
			FILE *outf = fopen("out.asm", "w+");
			if (not outf) {
				puts("Error: Cannot create assembly file.");
				return 1;
			}
			
			//Insert header
			switch (flag_trgt) {
				case OS_POSIX:
					fprintf(outf, "format ELF64 executable\n");
					fprintf(outf, "segment writeable\n");
					break;
				case OS_WIN32:
					fprintf(outf, "format PE64\n");
					fprintf(outf, "section '.idata' data readable import\n");
					fprintf(outf, "include 'WIN64A.INC'\n");
					fprintf(outf, "library \\\n");
					fprintf(outf, "k32, 'kernel32.dll', \\\n");
					fprintf(outf, "crt, 'msvcrt.dll' \n");
					fprintf(outf, "import k32, endnt, 'ExitProcess'\n");
					fprintf(outf, "import crt, printf, 'printf'\n");
					fprintf(outf, "section '.data' data readable writeable\n");
					break;
			}
			
			for (unsigned int i = 0; i < 4; ++i) {
				sstream *vl;
				switch (i) {
					case 0: vl = &v_b; bytesz = 'b'; break;
					case 1: vl = &v_w; bytesz = 'w'; break;
					case 2: vl = &v_d; bytesz = 'd'; break;
					case 3: vl = &v_q; bytesz = 'q'; break;
				}
				
				if (vl->size > 0)
				for (unsigned int i = 0; i < vl->size;) {
					ssinsert(vl, name, i++);
					ssinsert(vl, arrsz, i++);
					ssinsert(vl, value, i++);
					
					if (value[0] == '\x7f') fprintf(outf, "%-8s r%c %s\n",            name, bytesz, arrsz);        else
					if (atoi(arrsz) > 1)    fprintf(outf, "%-8s d%c %-4s dup (%s)\n", name, bytesz, arrsz, value); else
					if (atoi(arrsz) == 1)   fprintf(outf, "%-8s d%c %s\n",            name, bytesz, value);	
				}
			}
			
			//Insert code
			switch (flag_trgt) {
				case OS_POSIX:
					fprintf(outf, "segment executable\n");
					break;
				case OS_WIN32:
					fprintf(outf, "section '.text' code readable executable\n");
					break;
			}
			
			for (unsigned int i = 0; i < v_c.size;) {
				ssinsert(&v_c, value, i++);
				fprintf(outf, "%s", value);
			}
			
			//Save changes to output file
			fflush(outf);
			fclose(outf);
			
			puts("success!");
			
			//Print all stored variables
			if (flag_vrbs) {
				puts("Stored signed values:");   ssprint(&v_s);
				puts("Stored unsigned values:"); ssprint(&v_u);
				puts("Stored float values:");    ssprint(&v_f);
			}
			
			//Call FASM (Flat Assembler, download it on www.flatassembler.net)
			char fasmcall[64] = {0};
			
			strcat(fasmcall, "fasm out.asm ");
			strcat(fasmcall, argv[flag_outf]);
			strcat(fasmcall, " -m");
			sprintf(fasmcall + strlen(fasmcall), "%i", memc << (2 + 3 * (OS_DEFAULT != OS_POSIX)));
			
			if (flag_vrbs) printf("Call asm: '%s'\n", fasmcall);
			else           printf("Running FASM assembler...   ");
			
			switch (OS_DEFAULT) {
				case OS_POSIX:
					strcat(fasmcall, " > /dev/null");
					break;
				case OS_WIN32:
					strcat(fasmcall, " > NUL");
					break;
			}
			
			int fasm_retval = system(fasmcall);
			if (not (fasm_retval or flag_vrbs)) puts("success!");
			
			
			//switch (flag_trgt) {}
			//	case OS_POSIX: system("rm out.asm");  break;
			//	case OS_WIN32: system("del out.asm"); break;
			//
			
			//Free sstream and outfile memory
			printf("Freeing string memory...    ");
			if (v_s.stream) sstreamdel(&v_s);
			if (v_u.stream) sstreamdel(&v_u);
			if (v_f.stream) sstreamdel(&v_f);
			if (v_b.stream) sstreamdel(&v_b);
			if (v_w.stream) sstreamdel(&v_w);
			if (v_d.stream) sstreamdel(&v_d);
			if (v_q.stream) sstreamdel(&v_q);
			if (v_c.stream) sstreamdel(&v_c);
			puts("success!\n");
		}
	}
	
	//End of process
	if (argc < 2) puts("No input detected.\nInput 'terse -h' for help.\n");
	else          puts("Finished process.\n");
	
	return 0;
}
