#ifndef __X_MACRO_DEFINED__
#define __X_MACRO_DEFINED__

//Condition keywords (port to C99)
#define and &&
#define or  ||
#define xor ^
#define not !

//Sizes
#define BYTE 	1
#define WORD 	2
#define DWORD	4
#define QWORD	8

//Platform
#ifdef _WIN32
#define OS_DEFAULT 2
#endif

#ifdef __linux__
#define OS_DEFAULT 1
#endif

#define OS_POSIX	1
#define OS_LINUX	1
#define OS_UNIX 	1
#define OS_WIN32	2

#endif