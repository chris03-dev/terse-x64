//Modular main function processes

/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "macro.h"
#include "mproc.h"
#include "strproc.h"
#include "bintree.h"
#include "sstream.h"

int m_entry(char *lineref, sstream **v_list, int flag_trgt, int flag_fnc, int *flag_dclp, int flag_vrbs) {
	int read_state = 0;
	
	char word[16];
	tokenzr(word, lineref);
	
	//Check for word length
	read_state += m_fnlogic(lineref, v_list, flag_trgt);
	read_state += m_declvar(lineref, v_list, flag_fnc, flag_dclp, flag_vrbs); 
	read_state += m_compute(lineref, v_list, flag_vrbs, 0);
	
	return read_state;
}

int m_compute(char *lineref, sstream **v_list, int flag_vrbs, const char *reg) {
	//Tokens for compute
	char 
	line[256],
	token[3][32];
	
	//Flags
	unsigned int
	flag_op[2], 	//Operator (final operator, operator in loop)
	flag_num[2];	//Number flag (lvalue, rvalue)
	
	strcpy(line, lineref);
	
	if (find(line, "exec ") == 0) {
		trim(line, 5);
		
		if (flag_vrbs) puts("\n      EXECUTION START");
		
		unsigned char
		token_c   = 0;	//Token counter (0 to 2)
		
		node
		*root  = NULL,  	//Root node in binary tree
		*focus = NULL;  	//Node used for operator priority comparison
		
		//Structure binary tree
		node *n1, *n2, *n3;    //P.S. Use n1 as 1st node for post-order interval
		
		//Token loop
		do {
			//Get token from line
			tokenzr(token[token_c], line);
			trim(line, strlen(token[token_c]));
			
			//Check if number
			flag_num[token_c/2] = isnum(token[token_c]);
			
			if (flag_vrbs) printf("      Token %i: '%s'\n", token_c, token[token_c]);
			
			//Determine action in node counter
			switch (token_c) {
				case 2: 
					//Setup nodes
					if (not n1) 
					n1 = nodedef(token[0], 0);
					n2 = nodedef(token[1], 0);
					n3 = nodedef(token[2], 0);
					
					//Set operator value for current node
					switch (strlen(token[1])) {
						//Arithmetic/logic operators
						case 1:
							switch (token[1][0]) {
								case '=': n2->op_flag = OP_EQU;   break;
								case '^': n2->op_flag = OP_XOR;   break;
								case '*': n2->op_flag = OP_MUL_S; break;
								case '/': n2->op_flag = OP_DIV_S; break;
								case '+': n2->op_flag = OP_ADD_S; break;
								case '-': n2->op_flag = OP_SUB_S; break;
								default:
									printf("      Error: Invalid operator '%c' for execution.", token[1][0]);
									return 1;
							}
							break;
						
						//Conditional operators
						case 2:
							if (token[1][1] == '=') {
								switch (token[1][0]) {
									case '=': n2->op_flag = OP_EQU;   break;
									case '^': n2->op_flag = OP_XOR;   break;
									case '*': n2->op_flag = OP_MUL_S; break;
									case '/': n2->op_flag = OP_DIV_S; break;
									case '+': n2->op_flag = OP_ADD_S; break;
									case '-': n2->op_flag = OP_SUB_S; break;
									default:
										printf("      Error: Invalid operator '%s' for execution.", token[1]);
										return 1;
								}
							}
							break;
						default:
							printf("      Error: Invalid operator '%s' for execution.", token[1]);
							return 1;
					}
					
					//Link the nodes
					nodelink(n2, n1, NODE_PREV);
					nodelink(n2, n3, NODE_NEXT);
					
					//Check priority of operators for post-order traversal
					//Setup variables for token operator comparison
					node *focus_t = focus;
					
					unsigned char
					token_op,
					token_pop;
					
					//Check if first operation
					if (not root) {
						//Set root node in binary tree
						root = n2;
					}
					//Modify binary tree
					else {
						
						//Set previous operator to stored value in node
						token_op  = n2->op_flag;
						token_pop = n2->root->op_flag;
						
						//While priority is lower than previous operator,
						//check for upper operators
						/*while (token_op < token_pop 
						or     focus_t->root != NULL) {
							//Set root node
							focus_t = focus_t->root;
							//token_pop = n2->root->op_flag;
						}
						
						//Run when operator is high priority
						
						
						//Link the nodes
						nodelink(n2, n1, NODE_PREV);
						nodelink(n2, n3, NODE_NEXT);*/
					}
					
					
					//Set focus node for start of operator priority comparison
					if (not focus) focus = n2;
					
					//Reset token pointer
					token_c--;
					break;
				case 1:
				case 0:
					//Increment token pointer
					token_c++;
					break;
				
			}
		}
		while (strlen(line));
		
		//Write assembly code in post-order interval
		
		
		if (flag_vrbs) puts("      EXECUTION END\n");
	}
	
	return 0;
}

int m_fnlogic(char *lineref, sstream **v_list, int flag_trgt) {
	sstream *v_c = v_list[7];
	
	char 
	line[256] = {0},      	//Line string
	word[16][64] = {{0}}; 	//Token array
	unsigned int argc = 0;	//Argument count
	
	//Process line
	strcpy(line, lineref);
	trimw(line);
	
	//Get number of arguments
	for (;argc < 16 and strlen(line); ++argc) {
		strncpy(word[argc], line, findc(line, ','));
		trim(line, strlen(word[argc]) + 1);
	}
	
	//Find keywords in string
	//Function
	if (findb(lineref, "fn ", '"', '"', 0) == 0)  ssscanf(v_c, "%s:\n", word[0]); else
	if (findb(lineref, "nt ", '"', '"', 0) == 0) ssscanf(v_c, "entry %s\n%s:\n", word[0], word[0]);
	
	//Return value
	if (findb(lineref, "ret", '"', '"', 0) == 0)  ssgets(v_c, "ret\n\n"); else
	if (findb(lineref, "exit", '"', '"', 0) == 0)
	switch (flag_trgt) {
		case OS_POSIX:
			ssgets(v_c, "mov rax, 60\nsyscall\n\n");
			break;
		case OS_WIN32:
			ssgets(v_c, "mov rcx, 0\ncall [endnt]\n");
			break;
	}
	
	//Program code (*nix)
	if (findb(lineref, "align ", '"', '"', 0) == 0) 	ssscanf(v_c, "align %s\n", word[0]);
	if (findb(lineref, "sysc ", '"', '"', 0) == 0) {
		//Allocate stack arguments
		if (argc - 1 > 6) ssscanf(v_c, "sub rsp, %i\n", (argc - 7) * 8);
		for (int i = argc - 1; i >= 0; --i) {
			char reg[4];
			
			switch (i) {
				case 6: strcpy(reg, "r9");  break;
				case 5: strcpy(reg, "r8");  break;
				case 4: strcpy(reg, "rcx"); break;
				case 3: strcpy(reg, "rdx"); break;
				case 2: strcpy(reg, "rsi"); break;
				case 1: strcpy(reg, "rdi"); break;
				case 0: strcpy(reg, "rax"); break;
				default: reg[0] = (i - 6) * 8;
			}
			
			if (i > 6) ssscanf(v_c, "mov rax, %s\nmov [rsp+%i], rax\n", word[i], reg[0]);
			else       ssscanf(v_c, "mov %s, %s\n", reg, word[i]);
		}
		ssgets(v_c, "syscall\n\n");
		
		//Free stack arguments
		if (argc - 1 > 6) ssscanf(v_c, "add rsp, %i\n", (argc - 7) * 8);
	}
	if (findb(lineref, "nixc ", '"', '"', 0) == 0) {
		//Allocate stack arguments
		if (argc - 1 > 6) ssscanf(v_c, "sub rsp, %i\n", (argc - 7) * 8);
		for (unsigned int i = argc - 1; i; --i) {
			char reg[4];
			
			switch (i) {
				case 6: strcpy(reg, "r9");  break;
				case 5: strcpy(reg, "r8");  break;
				case 4: strcpy(reg, "rcx"); break;
				case 3: strcpy(reg, "rdx"); break;
				case 2: strcpy(reg, "rsi"); break;
				case 1: strcpy(reg, "rdi"); break;
				default: reg[0] = (i - 6) * 8;
			}
			
			if (i > 6) ssscanf(v_c, "mov rax, %s\nmov [rsp+%i], rax\n", word[i], reg[0]);
			else       ssscanf(v_c, "mov %s, %s\n", reg, word[i]);
		}
		ssscanf(v_c, "call %s\n\n", word[0]);
		
		//Free stack arguments
		if (argc - 1 > 6) ssscanf(v_c, "add rsp, %i\n", (argc - 7) * 8);
	}
	
	//Program code (w32)
	if (findb(lineref, "efic ", '"', '"', 0) == 0
	or  findb(lineref, "winc ", '"', '"', 0) == 0) {
		//Allocate stack arguments
		if (argc - 1 > 4) ssscanf(v_c, "sub rsp, %i\n", (argc - 5) * 8);
		for (unsigned int i = argc - 1; i; --i) {
			char reg[4];
			
			switch (i) {
				case 4: strcpy(reg, "r9");  break;
				case 3: strcpy(reg, "r8");  break;
				case 2: strcpy(reg, "rdx"); break;
				case 1: strcpy(reg, "rcx"); break;
				default: reg[0] = (i - 4) * 8;
			}
			
			if (i > 4) ssscanf(v_c, "mov rax, %s\nmov [rsp+%i], rax\n", word[i], reg[0]);
			else       ssscanf(v_c, "mov %s, %s\n", reg, word[i]);
		}
		
		ssscanf(v_c, "call %s\n\n", word[0]);
		
		//Free stack arguments
		if (argc - 1 > 4) ssscanf(v_c, "add rsp, %i\n", (argc - 5) * 8);
	}
	
	return 0;
}
int m_declvar(char *lineref, sstream **v_list, int flag_fnc, int *flag_dclp, int flag_vrbs) {
	sstream 
	*sst = 0,		//Stream pointer for type
	*ssz = 0,		//Stream pointer for size
	*v_s = v_list[0],
	*v_u = v_list[1],
	*v_f = v_list[2],
	*v_b = v_list[3],
	*v_w = v_list[4],
	*v_d = v_list[5],
	*v_q = v_list[6];
	
	char line[256];
	strcpy(line, lineref);
	
	unsigned int 
	sec_l = 0,  	//String length of section
	flag_dcl;   	//Saved declaration type and size
	
	//Check if not null pointer
	flag_dcl = *flag_dclp;	//Declaration code
	
	//Check if continuation of previous line
	if (flag_dclp and flag_dcl) {
		/*
			'flag_dcl' determines the saved type and size of declaration.
			If it is non-zero, it will execute the code below.
			
			1st 2 bits determine data type of sections:
			01 - Signed
			10 - Unsigned
			11 - Float
			
			2nd 2 bits determine data size of sections:
			00 - Byte
			01 - Word
			10 - Dword
			11 - Qword
			
			Therefore, sint32 is 1001, uint8 is 0001, float is 1011, double is 1111.
			
			THERE IS NO XX00 DATA TYPE.
			FLOAT TYPES CANNOT BE USE BYTE OR WORD SIZED.
		*/
		
		//Data typing
		switch (flag_dcl - (flag_dcl >> 2 << 2)) {
			case 3: sst = v_f; break;
			case 2: sst = v_u; break;
			case 1: sst = v_s; break;
			default:
				printf("      Undefined code in variable declaration type: %i.\n", flag_dcl - (flag_dcl >> 2 << 2));
				return 1;
		}
		
		//Data sizes
		switch (flag_dcl >> 2) {
			case 3: ssz = v_q; break;
			case 2: ssz = v_d; break;
			case 1: ssz = v_w; break;
			case 0: ssz = v_b; break;
			default:
				printf("      Undefined code in variable declaration size: %i.\n", flag_dcl - (flag_dcl >> 2 << 2));
				return 1;
		}
	}
	
	//Regular declaration
	else {
		//Data typing
		switch (line[0]) {
			case 'i':
			case 's': sst = v_s; flag_dcl = 1; break;
			case 'u': sst = v_u; flag_dcl = 2; break;
			case 'f': sst = v_f; flag_dcl = 3; break;
		}
		
		//Data sizes
		switch (line[1]) {
			case '1': ssz = v_b; break;
			case '2': ssz = v_w; flag_dcl += 1 << 2; break;
			case '4': ssz = v_d; flag_dcl += 2 << 2; break;
			case '8': ssz = v_q; flag_dcl += 3 << 2; break;
		}
		
		trim(line, 2);
	}
	
	
	if (sst != 0 and ssz != 0) {
		/*
			Max sizes of keywords are provided here.
			Error messages for these aren't implemented yet.
			
			Keyword           Chars (Max)
			Decl. section     256
			Variable name     64 
			Variable Value    128
		*/
		
		do {
			char 
			sec[256] = {0}, 	//String for section
			lex[128] = {0}, 	//String for tokens
			name[64] = {0}; 	//Name (Only for verbose flag purposes)
			
			unsigned int arrsz = 1;  	//Array size of variable
			
			//Move to next section
			trim(line, sec_l + (sec_l > 0));
			
			if (strlen(line)) {
				//Define sections
				sec_l = findcb(line, ',', '[', ']', 0);
				strncpy(sec, line, sec_l);
				
				//Jumpstart token
				tokenzr(lex, sec);
				
				//Array size
				while (isnum(lex)) {
					arrsz *= atoi(lex);
					trim(sec, strlen(lex));
					tokenzr(lex, sec);
				}
				
				//Variable name
				if (ssfindeq(v_s, lex) < v_s->size
				or  ssfindeq(v_u, lex) < v_u->size
				or  ssfindeq(v_f, lex) < v_f->size) {
					printf("      Error: Variable '%s' already defined.\n", lex);
					puts(v_s->stream);
					puts(v_u->stream);
					puts(v_f->stream);
					return 1;
				}
				
				//Declare variable name
				strcpy(name, lex);
				ssgets(sst, name);
				ssgets(ssz, name);
				ssscanf(ssz, "%i", arrsz);
				
				//Simply for printing variable declaration details
				if (flag_vrbs) {
					char 
					vrbs_type[9],
					vrbs_size[6];
					
					if (sst == v_s) strcpy(vrbs_type, "signed");
					if (sst == v_u) strcpy(vrbs_type, "unsigned");
					if (sst == v_f) strcpy(vrbs_type, "float");
					
					if (ssz == v_b) strcpy(vrbs_size, "byte");
					if (ssz == v_w) strcpy(vrbs_size, "word");
					if (ssz == v_d) strcpy(vrbs_size, "dword");
					if (ssz == v_q) strcpy(vrbs_size, "qword");
					
					printf("      Defined %s as %i %s %s(s).\n", name, arrsz, vrbs_type, vrbs_size);
				}
				
				//Value
				trim(sec, strlen(lex));
				tokenzr(lex, sec);
				
				//Equality validation
				if (lex[0] == '=' and lex[1] == '\0') {
					trim(sec, strlen(lex));
					tokenzr(lex, sec);
					
					//In case of array
					char buf[128] = {0};
					
					switch (lex[0]) {
						//Check if array
						case '[':
							if (findc(sec, ']') >= strlen(sec)) {
								puts("      Error: No closing brace for value found.");
								return 1;
							}
							else {
								strncpy(buf, sec + 1, findc(sec, ']') - 1);
								strcpy(lex, buf);
							}
							break;
						//Check if string
						case '"':
							strcat(lex, ", 0");
							break;
						//Check if number/predefined variable
						default:
							if (ssfindeq(v_s, lex) == v_s->size
							and ssfindeq(v_u, lex) == v_u->size
							and ssfindeq(v_f, lex) == v_f->size
							and not isnum(lex)) {
								printf("      Error: '%s' is not a valid value.", lex);
								return 1;
							}
							break;
					}
					printf("Value: %s\n", lex);
					ssgets(ssz, lex);
				}
				//Insert 0x7f to mark variable as no definition
				else if (not strlen(sec)) {
					ssgets(ssz, "\x7f");
				}
				else {
					printf("      Error: Invalid operator '%s' for variable definition.\n", lex);
					return 1;
				}
				
				//Save declaration in next line if no commas detected
				flag_dcl *= (findcb(line, ',', '[', ']', 0) < strlen(line));
				*flag_dclp = flag_dcl;
			
			}
		}
		while (strlen(line));
		if (flag_vrbs) puts("");
	}
	
	return 0;
}
