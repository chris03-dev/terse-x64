//Tree node
typedef struct node node;
struct node {
	node *root, *prev, *next;	//Link to other nodes
	char *value;             	//Name of node (if variable)
	char op_flag;                //Operator flag
};

#define NODE_ROOT 1
#define NODE_PREV 2
#define NODE_NEXT 3

//Operators
//Highest value is prioritized first
//and always not less than 0x80

	//Meta
	#define OP_ISVAR	0x80
	
	//In/decrement
	#define OP_INC  	0xea
	#define OP_DEC  	0xeb

	//Double arithmetic
	#define OP_ADD_F8	0xda
	#define OP_SUB_F8	0xdb
	#define OP_MUL_F8	0xdc
	#define OP_DIV_F8	0xdd
	
	//Float arithmetic
	#define OP_ADD_F4	0xca
	#define OP_SUB_F4	0xcb
	#define OP_MUL_F4	0xcc
	#define OP_DIV_F4	0xcd
	
	//Signed arithmetic
	#define OP_ADD_S	0xba
	#define OP_SUB_S	0xbb
	#define OP_MUL_S	0xbc
	#define OP_DIV_S	0xbd

	//Unsigned arithmetic
	#define OP_ADD_U	0xaa
	#define OP_SUB_U	0xab
	#define OP_MUL_U	0xac
	#define OP_DIV_U	0xad
	
	//Conditionals
	#define OP_AND  	0xa1
	#define OP_OR   	0xa2
	#define OP_NOT  	0xa3
	#define OP_XOR  	0xa4
	
	//Equality
	#define OP_EQU  	0xa0


node *nodedef(char *, int);          	//Create node using dynamic allocation
void nodedel(node *);                	//Delete all nodes using root node
int nodelink(node *, node *, int);   	//Insert node to position