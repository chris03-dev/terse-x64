//String stream implementation

/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "macro.h"
#include "sstream.h"
#include "strproc.h"

sstream sstreamdef(unsigned int n) {
	sstream s;
	s.size = 0;
	s.stream = calloc(n, 1);
	if (s.stream) s.ssize = n;
	return s;
}
void sstreamdel(sstream *ss) {
	if (ss) if (ss->stream) {
		free(ss->stream);
		ss->stream = 0;
	}
}
void ssflush(sstream *ss) {
	free(ss->stream);
	ss->stream = calloc(ss->ssize, 1);
	ss->size = 0;
}

void ssgets(sstream *ss, char *si) {
	//Increase memory if not enough
	while (ss->index[ss->size] + strlen(si) + 1 > ss->ssize) {
		printf("Warning: Exceeded memory limit (%#.2lx, > %#.2x allocated).\nIncreasing string heap memory to %#.2x...\n", ss->index[ss->size] + strlen(si) + 1, ss->ssize, 2 * ss->ssize);
		ss->ssize <<= 1;
		
		//Allocate new memory addresses
		void *stream = calloc(ss->ssize, 1);
		
		//Copy memory from struct to new addresses
		memcpy(stream, ss->stream, ss->ssize >> 1);
		
		//Free old addresses
		free(ss->stream);
		
		//Set pointer to new addresses
		ss->stream = stream;
	}
	
	//Write string to stream
	strcpy(ss->stream + ss->index[ss->size], si);
	
	//Struct changes
	ss->size++;
	ss->index[ss->size] = ss->index[ss->size - 1] + strlen(si);
}
void ssscanf(sstream *ss, char *si, ...) {
	va_list args;
	va_start(args, si);
	
	unsigned int maxsz = strlen(si) + snprintf(NULL, 0, "{data:%p}", (void *) args);
	
	char *buf = calloc(maxsz, 1);
	if (buf) {
		vsnprintf(buf, maxsz, si, args);
		
		while (ss->index[ss->size] + strlen(si) + 1 > ss->ssize) {
			printf("Warning: Exceeded memory limit (%#.2lx, > %#.2x allocated).\nIncreasing string heap memory to %#.2x...\n", ss->index[ss->size] + strlen(si) + 1, ss->ssize, 2 * ss->ssize);
			ss->ssize <<= 1;
			
			//Allocate new memory addresses
			void *stream = calloc(ss->ssize, 1);
			
			//Copy memory from struct to new addresses
			memcpy(stream, ss->stream, ss->ssize >> 1);
			
			//Free old addresses
			free(ss->stream);
			
			//Set pointer to new addresses
			ss->stream = stream;
		}
		snprintf(ss->stream + ss->index[ss->size], maxsz, "%s", buf);
	}
	va_end(args);
	
	//Struct changes
	ss->size++;
	ss->index[ss->size] = ss->index[ss->size - 1] + strlen(buf);
	free(buf);
}
char *ssputs(sstream *ss, unsigned int i) {
	return (char *) ss->stream + ss->index[i];
}

int ssfind(sstream *ss, char * si) {
	for (unsigned int i = 0, s = ss->size; i < s; ++i) {
		void * offset = ss->stream + ss->index[i];
		if (find(offset, si) == 0)
			return i;
	}
	return ss->size;
}
int ssfindeq(sstream *ss, char * si) {
	for (unsigned int i = 0, s = ss->size; i < s; ++i) {
		void * offset = ss->stream + ss->index[i];
		if (findeq(offset, si) == 0)
			return i;
	}
	return ss->size;
}

char *ssgetstream(sstream *ss) {
	return ss->stream;
}
void ssprint(sstream *ss) {
	char *buf = calloc(ss->ssize, 1);
	char *sss = ss->stream;
	unsigned int *ssi = ss->index;
	
	if (buf) {
		for (unsigned int i = 0, s = ss->size; i < s; ++i) {
			unsigned int s = ssi[i + 1] - ssi[i];
			strncpy(buf, sss + ssi[i], s);
			buf[s] = '\0';
			printf("%i:[%i] %s\n", i, ssi[i], buf);
		}
		free(buf);
	}
}

void ssinsert(sstream *ss, char *si, unsigned int i) {
	unsigned int *ssi = ss->index;
	unsigned int s = ssi[i + 1] - ssi[i];
	
	strncpy(si, ss->stream + ssi[i], s);
	si[s] = '\0';
}