#ifndef __X_SSTREAM_DEFINED__
#define __X_SSTREAM_DEFINED__

struct sstream {
	char *stream;	//The stringstream
	unsigned int
		index[4096],	//Points for accessing zero-trailing strings
		size,       	//Max current size of index
		ssize;      	//Max size of string stream
};

typedef struct sstream sstream;

sstream sstreamdef(unsigned int);               	//Properly create string stream contents
void sstreamdel(sstream *);                     	//Properly delete string stream contents
void ssflush(sstream *);                        	//Reset string stream

void ssgets(sstream *, char *);                 	//Insert string to string stream
void ssscanf(sstream *, char *, ...);           	//Insert formatted string to string stream
char *ssputs(sstream *, unsigned int);          	//Get string from index of string stream


int ssfind(sstream *, char *);                  	//Get index of substring in string stream
int ssfindeq(sstream *, char *);                	//Get index of null-terminated string in string stream

char *ssgetstream(sstream *);                   	//Get pointer of whole string stream
void ssprint(sstream *);                        	//Print string stream
void ssinsert(sstream *, char *, unsigned int); 	//Get string from index of string stream

/*
sstream sstreamdef(unsigned int);               	//Properly create string stream contents
void sstreamdel(sstream *);                     	//Properly delete string stream contents
void ssflush(sstream *);                        	//Reset string stream

void ssinsert(sstream *, char *);               	//Insert string to string stream
void ssinsertf(sstream *, char *, ...);         	//Insert formatted string to string stream
char *ssgetline(sstream *);                     	//Output whole string stream

int ssfind(sstream *, char *);                  	//Get index of substring in string stream
int ssfindeq(sstream *, char *);                	//Get index of substring in string stream
void ssputs(sstream *);                         	//Print string stream
void ssgets(sstream *, char *, unsigned int);   	//Get string from index of string stream
*/

#endif