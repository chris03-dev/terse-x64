//Binary tree implementation

/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bintree.h"

node* nodedef(char *si, int op_flag) {
	node *n = malloc(sizeof(node));
	
	//Make sure ptr value is 0; remove if not needed
	n->root = NULL;
	n->prev = NULL;
	n->next = NULL;
	
	n->value = calloc(32, 1);
	strncpy(n->value, si, (strlen(si) < 32) ? strlen(si) : 32);
	
	return n;
}
void nodedel(node *root) {
	if (root) {
		//Recursive nodedel to nodes
		if (root->prev) nodedel(root->prev);
		if (root->next) nodedel(root->next);
		
		//Free memory
		free(root->value);
		free(root);
	}
	
	root = NULL;
}

int nodelink(node *src, node *n, int where) {
	switch (where) {
		case NODE_ROOT: src->root = n; return 0;
		case NODE_PREV: src->prev = n; return 0;
		case NODE_NEXT: src->next = n; return 0;
		default: return 1;
	}
}