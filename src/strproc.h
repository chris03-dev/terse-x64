#ifndef __X_STRPROC_DEFINED__
#define __X_STRPROC_DEFINED__

void tokenzr(char *, const char *);                                      	//Get token
void trimw(char *);                                                      	//Remove 1st token from string
void trim(char *, unsigned int);                                         	//Remove whitespace from first and last characters
int find(const char *, const char *);                                    	//Find substring in string
int findeq(const char *, const char *);                                  	//Check string if equal
int findc(const char *, const char);                                     	//Find char in string
int findb(const char *, const char *, const char, const char, const int);	//Find substring in string only outside/inside blocks
int findcb(const char *, const char, const char, const char, const int); 	//Find char in string only outside/inside blocks

int countcb(const char *, const char, const char, const char, const int);	//Find char in string only outside/inside blocks


int isnum(const char *);	//Check if valid number/hex
void preproc(char *);   	//Used for compiler comment purposes

#endif