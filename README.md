# terse-x64

FASM based compiler, written in C99. Based on a previous project, [flathill](https://github.com/chris03-dev/flathill-x64).
This repository provides a set of tools for compilation of .mp files into executables.

### How to Compile
1. Install a C compiler compatible with the C99 standard
	- Windows: [LLVM-clang](https://releases.llvm.org/), [MinGW](https://sourceforge.net/projects/mingw/), [MinGW-w64](http://mingw-w64.org/doku.php/download/)
	- Linux: `sudo apt-get install clang` or `sudo apt-get install gcc`
2. Download the source code [directly](https://codeberg.org/vivid-lang/vivid-x64/archive/main.zip) or input `git clone https://codeberg.org/chris03-dev/vivid-x64` in your text-based interface of choice
3. Navigate to the `scr/` directory
4. Run the appropriate script, depending on the operating system
	- `compile-w32llvm.bat` (clang) or `compile-w32gcc.bat` (gcc) for Windows
	- `compile-nixllvm.sh` (clang) or `compile-nixgcc.sh` (gcc) for Linux/Unix

### How to Use
`terse <outf> <inf or flag> ...` in your text-based interface of choice. <br>
`terse -h` shows most of what can be used with the .tr executable.