printf "\nRunning *nix clang debug build script...\n"
cd $(dirname "$0")
mkdir -p ../bin
cd ../src
if clang -Wall -DNO_OLDNAMES -std=c99 main.c strproc.c sstream.c mproc.c bintree.c -s -o ../bin/terse; then
    printf "Build successful.\n\n"
else
    printf "Build failure.\n\n"
fi
