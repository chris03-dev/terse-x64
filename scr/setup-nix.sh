printf "\nRunning *nix setup...\n"
cd $(dirname "$0")

if [ `whoami` != root ]; then
	echo "Cannot write to '/usr/local/bin/' due to non-root bash command."
	printf "Run the script as root user or input 'sudo bash <file>'.\n\n"
exit
else
	if cp ../bin/mpic /usr/local/bin/terse; then
		echo "Setup successful."
		echo "You may now call 'terse' from anywhere in your Linux terminal."
	else
		echo "Setup failed."
	fi
	printf "\n"
fi
