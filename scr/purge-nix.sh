
printf "\nRunning *nix remove...\n"
cd $(dirname "$0")

if [ `whoami` != root ]; then
    echo "Cannot write to 'usr/local/bin' due to non-root bash command."
    printf "Run the script as root user or input 'sudo bash <file>'.\n\n"
exit
else
    if rm /usr/local/bin/terse; then
        echo "Deletion successful."
    else
        echo "Deletion failed."
    fi
    printf "\n\n"
fi
