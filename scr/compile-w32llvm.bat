@echo off

echo.
echo Running Windows clang build script...

if not exist ..\bin\ mkdir ..\bin\
cd ..\src\
windres win32res.rc -O coff -o win32res.res
clang -Wall -s -O2 -std=c99 -target x86_64-pc-windows-gnu -Wl,--gc-sections main.c strproc.c sstream.c mproc.c bintree.c -o ..\bin\terse.exe

if %ERRORLEVEL% neq 0 (echo Build failure.) else (echo Build successful.)
echo.
