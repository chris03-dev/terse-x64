@echo off

echo.
echo Running Windows GCC build script...

if not exist ..\bin\ mkdir ..\bin\
cd ..\src\
windres win32res.rc -O coff -o win32res.res
gcc -Wall -s -O2 -std=c99 -Wl,--gc-sections main.c strproc.c sstream.c mproc.c bintree.c -o ..\bin\terse.exe

if %ERRORLEVEL% neq 0 (echo Build failure.) else (echo Build successful.)
echo.
