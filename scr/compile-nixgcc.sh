printf "\nRunning *nix GCC build script...\n"
cd $(dirname "$0")
mkdir -p ../bin
cd ../src
if gcc -Wall -s -Os -O2 -std=c99 -DNO_OLDNAMES -Wl,--gc-sections main.c strproc.c sstream.c mproc.c bintree.c -s -o ../bin/terse; then
    printf "Build successful.\n\n"
else
    printf "Build failure.\n\n"
fi
